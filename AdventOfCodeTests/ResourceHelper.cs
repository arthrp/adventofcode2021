using System.IO;
using System.Reflection;

namespace AdventOfCodeTests;

public class ResourceHelper
{
    public static string ReadAsString(string resourcePath)
    {
        var assembly = Assembly.GetExecutingAssembly();

        using (var stream = assembly.GetManifestResourceStream(resourcePath))
        using (var reader = new StreamReader(stream))
        {
            var result = reader.ReadToEnd();
            return result;
        }
    }
}