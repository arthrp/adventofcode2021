using AdventOfCode2021;
using NUnit.Framework;

namespace AdventOfCodeTests;

[TestFixture]
public class ProblemOneTests
{
    private static string _input;
    
    [OneTimeSetUp]
    public void Setup()
    {
        _input = ResourceHelper.ReadAsString("AdventOfCodeTests.input1_small.txt");
    }
    
    [Test]
    public void BasicTask_SolvedCorrectly()
    {
        var p = new ProblemOne(_input);
        Assert.AreEqual(7, p.Solve());
    }

    [Test]
    public void AdditionalTask_SolvedCorrectly()
    {
        var p = new ProblemOne(_input);
        Assert.AreEqual(5, p.SolveAdditional());
    }
}