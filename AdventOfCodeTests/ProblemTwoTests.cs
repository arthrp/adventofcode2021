using System.IO;
using System.Reflection;
using AdventOfCode2021;
using NUnit.Framework;

namespace AdventOfCodeTests;

[TestFixture]
public class ProblemTwoTests
{
    private static string _input;
    [OneTimeSetUp]
    public void Setup()
    {
        _input = ResourceHelper.ReadAsString("AdventOfCodeTests.input2_small.txt");
    }

    [Test]
    public void BasicSolving_Works()
    {
        var p = new ProblemTwo(_input);
        var result = p.Solve();
        
        Assert.AreEqual(150, result);
    }

    [Test]
    public void AdditionalTask_SolvedCorrectly()
    {
        var p = new ProblemTwo(_input);
        var result = p.Solve(true);
        
        Assert.AreEqual(900, result);
    }
}