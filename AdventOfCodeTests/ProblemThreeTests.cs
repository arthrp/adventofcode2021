using AdventOfCode2021;
using NUnit.Framework;

namespace AdventOfCodeTests;

public class ProblemThreeTests
{
    private static string _input;
    [OneTimeSetUp]
    public void Setup()
    {
        _input = ResourceHelper.ReadAsString("AdventOfCodeTests.input3_small.txt");
    }

    [Test]
    public void BasicTask_SolvedCorrectly()
    {
        var p = new ProblemThree(_input);
        
        Assert.AreEqual(198, p.Solve());
    }

    [Test]
    public void BasicTask_SolvedCorrectlyWithLinq()
    {
        var p = new ProblemThree_Other(_input);
        
        Assert.AreEqual(198, p.SolveWithLinq());
    }

    [Test]
    public void AdditionalTask_SolvedCorrectly()
    {
        var p = new ProblemThree(_input);
        
        Assert.AreEqual(230, p.SolveAdditional());
    }
}