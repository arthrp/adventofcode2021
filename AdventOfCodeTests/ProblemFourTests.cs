using AdventOfCode2021;
using NUnit.Framework;

namespace AdventOfCodeTests;

public class ProblemFourTests
{
    private static string _input;
    
    [OneTimeSetUp]
    public void Setup()
    {
        _input = ResourceHelper.ReadAsString("AdventOfCodeTests.input4_small.txt");
    }
    
    [Test]
    public void BasicTask_SolvedCorrectly()
    {
        var p = new ProblemFour(_input);
        Assert.AreEqual(4512, p.Solve());
    }
    
    [Test]
    public void AdditionalTask_SolvedCorrectly()
    {
        var p = new ProblemFour(_input);
        Assert.AreEqual(1924, p.Solve(true));
    }
}