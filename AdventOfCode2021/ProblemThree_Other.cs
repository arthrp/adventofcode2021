using System.Text;

namespace AdventOfCode2021;

/// <summary>
/// Other solutions for Problem 3
/// </summary>
public class ProblemThree_Other
{
    private readonly List<string> _inputs;
    
    public ProblemThree_Other(string input)
    {
        if (input.EndsWith('\n'))
        {
            input = input.Remove(input.Length - 1);
        }
        
        _inputs = input.Split('\n').ToList();
    }

    public int SolveWithLinq()
    {
        var gammaRateStr = new StringBuilder();
        var epsilonRateStr = new StringBuilder();
        var symbolsPerInput = _inputs[0].Length; //Strings are the same length
        for (int i = 0; i < symbolsPerInput; i++)
        {
            var s = new StringBuilder();
            foreach (var input in _inputs)
            {
                s.Append(input[i]);
            }

            var mostCommon = GetMostCommonNumLinq(s.ToString().ToCharArray());
            var leastCommon = (mostCommon == '1') ? '0' : '1';
            gammaRateStr.Append(mostCommon);
            epsilonRateStr.Append(leastCommon);
        }

        var gammaRate = Convert.ToInt32(gammaRateStr.ToString(), 2);
        var epsRate = Convert.ToInt32(epsilonRateStr.ToString(), 2);
        return gammaRate*epsRate;
    }

    private char GetMostCommonNumLinq(char[] arr)
    {
        var counts = arr.Aggregate(new Dictionary<int, int>(){ {0, 0}, {1, 0} }, 
            (acc, c) =>
            {
                var n = c == '0' ? 0 : 1;
                acc[n] += 1;
                return acc;
        });

        return counts[0] > counts[1] ? '0' : '1';
    }
}