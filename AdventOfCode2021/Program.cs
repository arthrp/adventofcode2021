﻿namespace AdventOfCode2021;

public static class Program
{
    public static void Main(string[] args)
    {
        string path = args[0];
        
        var result = new ProblemOne(File.ReadAllText(path)).SolveAdditional();
        Console.WriteLine(result);
    }
}