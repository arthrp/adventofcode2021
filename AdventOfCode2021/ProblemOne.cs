namespace AdventOfCode2021;

public class ProblemOne
{
    private readonly List<int> _measurements = new();
    
    public ProblemOne(string fileContents)
    {
        if (fileContents.EndsWith('\n'))
        {
            fileContents = fileContents.Remove(fileContents.Length - 1);
        }
        
        var l = fileContents.Split('\n');
        _measurements = l.Select(int.Parse).ToList();
    }

    public int Solve()
    {
        var count = 0;

        for (int i = 1; i < _measurements.Count; i++)
        {
            count += (_measurements[i] > _measurements[i - 1] ? 1 : 0);
        }
        
        return count;
    }

    public int SolveAdditional()
    {
        var count = 0;

        for (int lastIndex = 3; lastIndex < _measurements.Count; lastIndex++)
        {
            var prevLastIndex = lastIndex - 1;
            var prevSum = _measurements[prevLastIndex] + _measurements[prevLastIndex - 1] +
                      _measurements[prevLastIndex - 2];
            var sum = _measurements[lastIndex] + _measurements[lastIndex - 1] + _measurements[lastIndex - 2];

            count += (sum > prevSum) ? 1 : 0;
        }

        return count;
    }
}