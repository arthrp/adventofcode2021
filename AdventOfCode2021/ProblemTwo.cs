namespace AdventOfCode2021;

public class ProblemTwo
{
    private readonly List<string> _inputs;
    public ProblemTwo(string input)
    {
        if (input.EndsWith('\n'))
        {
            input = input.Remove(input.Length - 1);
        }
        
        _inputs = input.Split('\n').ToList();
    }

    public long Solve(bool isAdditionalTask = false)
    {
        var p = new Position() { HorizontalPos = 0, Depth = 0 };

        foreach (var line in _inputs)
        {
            if(isAdditionalTask) ProcessInputAdditional(line, p); else ProcessInput(line, p);
        }
        return p.Depth * p.HorizontalPos;
    }

    private void ProcessInput(string inputLine, Position position)
    {
        var contents = inputLine.Split(' ');
        var num = int.Parse(contents[1]);

        (int,int) delta = contents[0] switch
        {
            "forward" => (num,0),
            "down" => (0,num),
            "up" => (0,-num),
            _ => throw new ArgumentException("Unexpected command"),
        };

        position.HorizontalPos += delta.Item1;
        position.Depth += delta.Item2;
    }

    private void ProcessInputAdditional(string inputLine, Position position)
    {
        var contents = inputLine.Split(' ');
        var num = int.Parse(contents[1]);

        (int, int, int) delta = contents[0] switch
        {
            "forward" => (num,position.Aim*num,0),
            "down" => (0,0,num),
            "up" => (0,0,-num),
            _ => throw new ArgumentException("Unexpected command"),
        };
        
        position.HorizontalPos += delta.Item1;
        position.Depth += delta.Item2;
        position.Aim += delta.Item3;
    }
}

class Position
{
    public long HorizontalPos { get; set; }
    public long Depth { get; set; }
    public int Aim { get; set; }
}