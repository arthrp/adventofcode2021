using System.Text;

namespace AdventOfCode2021;

public class ProblemThree
{
    private readonly List<string> _inputs;
    public ProblemThree(string input)
    {
        if (input.EndsWith('\n'))
        {
            input = input.Remove(input.Length - 1);
        }
        
        _inputs = input.Split('\n').ToList();
    }

    public int Solve()
    {
        var gammaRateStr = new StringBuilder();
        var epsilonRateStr = new StringBuilder();
        var symbolsPerInput = _inputs[0].Length; //Strings are the same length
        for (int i = 0; i < symbolsPerInput; i++)
        {
            var s = new StringBuilder();
            foreach (var input in _inputs)
            {
                s.Append(input[i]);
            }

            var mostCommon = GetMostCommonNum(s.ToString().ToCharArray());
            var leastCommon = (mostCommon == '1') ? '0' : '1';
            gammaRateStr.Append(mostCommon);
            epsilonRateStr.Append(leastCommon);
        }

        var gammaRate = Convert.ToInt32(gammaRateStr.ToString(), 2);
        var epsRate = Convert.ToInt32(epsilonRateStr.ToString(), 2);
        return gammaRate*epsRate;
    }

    public int SolveAdditional()
    {
        var oxygenGenRating = GetRating(RatingType.OxygenGeneratorRating);
        var co2ScrubberRating = GetRating(RatingType.Co2ScrubberRating);

        var result = oxygenGenRating * co2ScrubberRating;

        return result;
    }

    private int GetRating(RatingType type)
    {
        var inputs = _inputs;
        var index = 0;
        while (inputs.Count > 1)
        {
            var nthBits = inputs.Aggregate(new StringBuilder(), (acc, e) => acc.Append(e[index])).ToString();

            var critereonChar = type switch
            {
                RatingType.OxygenGeneratorRating => GetMostCommonNum(nthBits.ToCharArray(), true),
                RatingType.Co2ScrubberRating => GetLeastCommonNum(nthBits.ToCharArray())
            };
            inputs = inputs.Where(i => i[index] == critereonChar).ToList();

            if (index >= inputs[0].Length) throw new ArgumentException("Invalid data");
            index++;
        }

        return Convert.ToInt32(inputs[0], 2);
    }

    private char GetMostCommonNum(char[] arr, bool isAdditionalTask = false)
    {
        int ones = 0, zeros = 0;
        
        foreach (var c in arr)
        {
            if (c == '1') ones++;
            else zeros++;
        }
        
        return (isAdditionalTask) ? (ones >= zeros ? '1' : '0') :
            (ones > zeros ? '1' : '0');
    }

    private char GetLeastCommonNum(char[] arr)
    {
        int ones = 0, zeros = 0;

        foreach (var c in arr)
        {
            if (c == '1') ones++;
            else zeros++;
        }

        return zeros <= ones ? '0' : '1';
    }

    enum RatingType
    {
        OxygenGeneratorRating,
        Co2ScrubberRating
    }
}