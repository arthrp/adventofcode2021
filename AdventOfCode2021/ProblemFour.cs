namespace AdventOfCode2021;

public class ProblemFour
{
    private readonly IEnumerable<int> _drawnNumbers;
    // Board id : Rows+Columns
    private readonly Dictionary<int, List<List<Cell>>> _boards = new();

    public ProblemFour(string input)
    {
        var lines = input.Split('\n');
        _drawnNumbers = lines[0].Split(',').Select(int.Parse);
        var boardIndex = 0;

        var b = new List<string>();
        for (int i = 2; i < lines.Length; i++)
        {
            if (lines[i] == "" || i == lines.Length-1)
            {
                var rowsAndCols = InitBoard(b);
                _boards[boardIndex] = rowsAndCols;
                
                b = new List<string>();
                boardIndex++;
                continue;
            }
            
            b.Add(lines[i]);
        }
    }

    public int Solve(bool isAdditionalTask = false)
    {
        var (winningNum, winningBoard) = (isAdditionalTask) ? CallNumbersAdditional() : CallNumbers();
        var unmarkedSum = GetUnmarkedSum(winningBoard);
        
        return unmarkedSum * winningNum;
    }

    private List<List<Cell>> InitBoard(List<string> boardInput)
    {
        var result = new List<List<Cell>>();
        foreach (var line in boardInput)
        {
            var els = line.Split(' ').Where(e => !string.IsNullOrWhiteSpace(e));
            var l = els.Select(e => new Cell() { Number = int.Parse(e.Trim()), IsMarked = false });
            result.Add(l.ToList());
        }
        var other = new List<List<Cell>>();
        for (int i = 0; i < result[0].Count; i++)
        {
            var x = result.Select(t => new Cell() { Number = t[i].Number, IsMarked = false }).ToList();
            other.Add(x);
        }

        result = result.Concat(other).ToList();
        return result;
    }

    private (int,int) CallNumbers()
    {
        foreach (var number in _drawnNumbers)
        {
            MarkNumber(number);
            var won = GetWinningBoard();

            if (won.HasValue)
            {
                return (number, won.Value);
            }
        }

        throw new ArgumentException("Invalid data, no board has won");
    }

    private (int, int) CallNumbersAdditional()
    {
        var wonBoards = new List<int>();
        var wonNumber = -1;
        foreach (var number in _drawnNumbers)
        {
            MarkNumber(number, wonBoards);
            var won = GetAllWinningBoards(wonBoards);

            if (won.Count > 0)
            {
                wonBoards = wonBoards.Concat(won).ToList();
                wonNumber = number;
                if(wonBoards.Count != _boards.Count) continue;
                return (number, wonBoards.Last());
            }
        }

        return (wonNumber, wonBoards.Last());
    }
    
    private void MarkNumber(int n, List<int>? wonBoards = null)
    {
        var boards = wonBoards != null ? _boards.Where(kv => !wonBoards.Contains(kv.Key)) : _boards;
        foreach (var board in boards)
        {
            foreach (var rowOrColumn in board.Value)
            {
                foreach (var cell in rowOrColumn)
                {
                    if (cell.Number == n) cell.IsMarked = true;
                }
            }
        }
    }

    private int? GetWinningBoard()
    {
        foreach (var (key, value) in _boards)
        {
            var hasWon = value.Any(rc => rc.All(c => c.IsMarked));
            if (hasWon) return key;
        }

        return null;
    }

    private List<int> GetAllWinningBoards(List<int>? wonBoards)
    {
        var boards = _boards.Where(b => !wonBoards.Contains(b.Key));
        var result = new List<int>();

        foreach (var (key,value) in boards)    
        {
            var hasWon = value.Any(rc => rc.All(c => c.IsMarked));
            if (hasWon) result.Add(key);
        }

        return result;
    }

    private int GetUnmarkedSum(int boardIndex)
    {
        var board = _boards[boardIndex];
        var sum = 0;
        //Let's take only rows, so indices 0 - 4
        for (int i = 0; i < 5; i++)
        {
            sum += board[i].Where(e => !e.IsMarked).Sum(e => e.Number);
        }
        
        return sum;
    }
}

internal class Cell
{
    public int Number { get; init; }
    public bool IsMarked { get; set; }
}