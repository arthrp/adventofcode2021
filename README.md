# Advent of Code 2021

This repo contains solutions to Advent of code 2021 tasks in C# along with tests.
For each problem solved, there exists a `ProblemN.cs` file which contains
the 'canon' solution. There also may exist `ProblemN_Other.cs` file which 
contains other solutions to the same problem.

The goal is also to have no external dependencies.
